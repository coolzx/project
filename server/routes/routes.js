
const router=require('express').Router();
const jwt = require('jsonwebtoken');
const User=require('../model/User')
const bcrypt = require('bcrypt');
const {registerValidation,loginValidation}=require('../validations')

const saltRounds = 10;


router.get('/', (req, res) => res.send('Hello World!'))


router.post('/api/register',async (req, res) => {  
    let {name,email,password}=req.body
    const {error}=registerValidation(req.body)
    if(error)
        return res.status(400).send(error.details[0].message)
    const emailExist=User.findOne({email})
    if(emailExist)
        return res.status(400).send("already exist.")


    const salt=await bcrypt.genSalt(saltRounds);
    const hashPassword=await bcrypt.hash(password,salt)
    
    const user=new User({
            name,
            email,
            password:hashPassword,
    })

    try{
        const saveUser=await user.save();
        res.send(saveUser);
    }
    catch(err){
        req.status(400).send(err)
    }
  });

  router.post('/api/login', async (req, res) => {
    let {name,email,password}=req.body
    const {error}=registerValidation(req.body)
    if(error)
        return res.status(400).send(error.details[0].message)
    const user=User.findOne({email})
    if(!emailExist)
        return res.status(400).send("email doesnt exist.")
    const validPass=await bcrypt.compare(password, user.password);
    if(!validPass)
        return res.status(400).send("invalid password.")


    jwt.sign({user}, process.env.TOKEN_SECRET, { expiresIn: EXPIRIATION_TIME}, (err, token) => {
      res.json({
        token
      });
    });
  });


router.post('/api/posts', verifyToken, (req, res) => {  
    jwt.verify(req.token,process.env.TOKEN_SECRET, (err, authData) => {
      if(err) {
        res.sendStatus(403);
      } else { 
        res.json({
          message: 'Post created...',
          authData
        });
      }
    });
  });



  // Verify Token
  function verifyToken(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if(typeof bearerHeader !== 'undefined') {
      // Split at the space
      const bearer = bearerHeader.split(' ');
      // Get token from array
      const bearerToken = bearer[1];
      // Set the token
      req.token = bearerToken;
      // Next middleware
      next();
    } else {
      // Forbidden
      res.sendStatus(403);
    }
  
  }  

module.exports=router;