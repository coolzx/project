


import Browse from './Browse'
import Explore from './Explore'
import Login from './Login'
import Product from './Product'
import Settings from './Settings'
import Welcome from './Welcome'
import SignUp from './SignUp'
import preSignUp from './preSignUp'
import Advice from './Advice'


module.exports={Browse,Explore,Login,Product,Settings,Welcome,SignUp,preSignUp,Advice}
