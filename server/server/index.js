const express=require('express')
const mongoose = require('mongoose');
const dotenv=require('dotenv')
const routes=require('./routes/routes')
const app=express();

const EXPIRIATION_TIME=60*60;
const PORT=process.env.PORT|| 5000;


dotenv.config()

app.use(express.json())

//Routes
app.use('/',routes)


//Enable CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Headers","GET, POST, PUT, DELETE, OPTIONS")
    if(req.method==="OPTIONS")
    {
        res.status(200).send()
    }
    else{
        next();
    }
  });


mongoose.connect(process.env.DB_CONNECT, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => console.log('MongoDB Connected'))
.catch(err => console.log(err));;


app.listen(PORT);
