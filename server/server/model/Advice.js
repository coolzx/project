const mongoose = require('mongoose');
const { Schema } = mongoose



const adviceSchema = new Schema({
    type: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
        min: 10,
    },
    email:{
        type:String,
        required:true,
        max:255,
        min:6
    },
    placeName: {
        type: String,
        required: true,
        min: 10,
    },
    city: {
        type: String,
        required: true,
    },
    country: {
        type: String,
        required: true,
    },
    
    description: {
        type: String,
        required: true,
        max: 255,
        min: 6
    },
    rating: {
        type: String,
        required: true,
        max: 1
    },
    date: {
        type: Date,
        default: Date.now
    }
});


module.exports = mongoose.model('advices', adviceSchema);