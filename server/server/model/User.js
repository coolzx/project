const mongoose = require('mongoose');
const {Schema}=mongoose



const userSchema=new Schema({

phone:{
    type:String,
    required:true,
    min:10,
},
email:{
    type:String,
    required:true,
    max:255,
    min:6
},
password:{
    type:String,
    required:true,
    min:6,
    max:1024
},
confirmPassword:{
    type:String,
    required:true,
    min:6,
    max:1024
},
userContactsMember:{
    type:String,
    required:true,
},
date:{
    type:Date,
    default:Date.now
}
});


module.exports=mongoose.model('users',userSchema);