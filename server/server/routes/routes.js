
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const User = require('../model/User')
const Advice = require('../model/Advice')

const bcrypt = require('bcrypt');

const { registerValidation, loginValidation, adviceValidation } = require('../validations')
const EXPIRIATION_TIME = 60 * 60;
const saltRounds = 10;



router.post('/api/register', async (req, res) => {
    let { phone, email, password, confirmPassword, userContactsMember } = req.body
    const createUser = new Promise((resolve, reject) => {
        const error = registerValidation(req.body)
        console.log("errorVal", error)
        if (error)
            return reject("Wrong input")
        User.findOne({ email })
            .then(async (user) => {
                if (user)
                    return reject("already exist.")
                const salt = await bcrypt.genSalt(saltRounds);
                const hashPassword = await bcrypt.hash(password, salt)
                const hashConfirmPassword = await bcrypt.hash(confirmPassword, salt)

                const nUser = new User({
                    phone,
                    email,
                    password: hashPassword,
                    confirmPassword: hashConfirmPassword,
                    userContactsMember
                })
                try {
                    const saveUser = await nUser.save();
                    resolve(saveUser)
                }
                catch (err) {
                    reject(err)

                }
            })
    })
    createUser
        .then((user) => {
            res.send(user)
        })
        .catch((err) => {
            res.status(400).send(err)
        })



});

router.post('/api/advice', async (req, res) => {
    let { placeName, description, rating, phone, email ,  city,  country,type} = req.body
    console.log("errorVal", placeName, description, rating, phone, email)
    const createAdvice = new Promise(async(resolve, reject) => {
        console.log("enter createAdvice promises")
        /*
        let error = adviceValidation(req.body)
        console.log("heeerrreeee1",error)
        if (error)
            return reject("Wrong input")
        */
        const nAdvice = new Advice({
            phone,
            city,
            country,
            email,
            placeName,
            description,
            rating,
            type
        })

        console.log("newAdvice", nAdvice)
        try {
            const saveAdvice = await nAdvice.save();
            console.log("finished successfully")
            resolve(saveAdvice)
        }
        catch (err) {
            console.log("failed to save advice")
            reject(err)

        }

    })
    createAdvice
        .then((user) => {
            res.send(user)
        })
        .catch((err) => {
            res.status(400).send(err)
        })



});

router.post('/api/login', async (req, res) => {
    let { email, password } = req.body
    const loginUser = new Promise((resolve, reject) => {
        //const error = loginValidation(req.body)
      //  if (error)
      //      return reject("Wrong input")
        User.findOne({ email })
            .then(async (user) => {
                console.log("printuser", user)
                if (!user)
                    return reject("email doesnt exist.")
                const validPass = await bcrypt.compare(password, user.password);
                console.log("comparepassword", validPass, password)
                if (!validPass)
                    return reject("invalid password.")

                jwt.sign({ user }, process.env.TOKEN_SECRET, { expiresIn: EXPIRIATION_TIME }, (err, token) => {
                    res.json({
                        token
                    });
                });
            })
    })
    loginUser.then((user) => {
        res.send(user)
    })
        .catch((err) => {
            console.log("checkckckckckc", req, err)
            return res.status(400).send(err)
        })

});


router.post('/api/posts', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.TOKEN_SECRET, (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            res.json({
                message: 'Post created...',
                authData
            });
        }
    });
});



// Verify Token
function verifyToken(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        // Next middleware
        next();
    } else {
        // Forbidden
        res.sendStatus(403);
    }

}

module.exports = router;